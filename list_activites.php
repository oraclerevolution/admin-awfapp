<?php
session_start();
require './database.php';
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Meta and Title -->
    <meta charset="utf-8">
    <title>AWFAPP - Programme du mercredi</title>
    <meta name="keywords" content="HTML5, Bootstrap 3, Admin Template, UI Theme" />
    <meta name="description" content="AWFAPP - L'application administrative de gestion de l'application AWFAPP">
    <meta name="author" content="ThemeREX">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- Angular material -->
    <link rel="stylesheet" type="text/css" href="assets/skin/css/angular-material.min.css">
    <!-- Icomoon -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/icomoon/icomoon.css">
    <!-- AnimatedSVGIcons -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/animatedsvgicons/css/codropsicons.css">
    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/magnific/magnific-popup.css">
    <!-- c3charts -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/c3charts/c3.min.css">
    <!-- CSS - allcp forms -->
    <link rel="stylesheet" type="text/css" href="assets/allcp/forms/css/forms.css">
    <!-- mCustomScrollbar -->
    <link rel="stylesheet" type="text/css" href="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css">
    <!-- CSS - theme -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/less/theme.css">
</head>
<body class="dashboard-page with-customizer">
    <!-- Body Wrap  -->
    <div id="main">
        <!-- Header  -->
        <?php include "./partials/top-header.php"; ?>
        <!-- /Header -->
        <!-- Sidebar  -->
        <?php include "./partials/side-header.php"; ?>
        <!-- /Sidebar -->
        <!-- Main Wrapper -->
        <section id="content_wrapper">
            <!-- Topbar -->
            <header id="topbar" class="alt">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-link">
                            <a href="accueil.php">Accueil</a>
                        </li>
                        <li class="breadcrumb-current-item">Tableau de bord</li>
                    </ol>
                </div>
            </header>
            <!-- /Topbar -->
            <!-- Content -->
            <section id="content" class="container col-lg-12">
                    <div class="col-lg-12">
                        <h3>Liste des activites du mercredi</h3>
                        <table class="table table-bordered">
                            <thead>
                            <th>Heure</th>
                            <th>Titre</th>
                            <th>Jour</th>
                            <th>Description</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $bdd = Database::connect();
                            $activite = $bdd->query('SELECT * FROM activites WHERE id_jour=1');
                            while ($donnees = $activite->fetch()) {
                                ?>
                                <tr>
                                    <td><?php echo $donnees['heure']; ?></td>
                                    <td><?php echo $donnees['title']; ?></td>
                                    <td><?php echo $donnees['id_jour']; ?></td>
                                    <td><?php echo $donnees['description']; ?></td>
                                    <td>
                                        <a href="update-activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-info">modifier</button>
                                        </a>
                                        <a href="delete_activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-danger">supprimer</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                            Database::disconnect();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <h3>Liste des activites du jeudi</h3>
                        <table class="table table-bordered">
                            <thead>
                            <th>Heure</th>
                            <th>Titre</th>
                            <th>Jour</th>
                            <th>Description</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $bdd = Database::connect();
                            $activite = $bdd->query('SELECT * FROM activites WHERE id_jour=2');
                            while ($donnees = $activite->fetch()) {
                                ?>
                                <tr>
                                    <td><?php echo $donnees['heure']; ?></td>
                                    <td><?php echo $donnees['title']; ?></td>
                                    <td><?php echo $donnees['id_jour']; ?></td>
                                    <td><?php echo $donnees['description']; ?></td>
                                    <td>
                                        <a href="update-activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-info">modifier</button>
                                        </a>
                                        <a href="delete_activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-danger">supprimer</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                            Database::disconnect();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <h3>Liste des activites du vendredi</h3>
                        <table class="table table-bordered">
                            <thead>
                            <th>Heure</th>
                            <th>Titre</th>
                            <th>Jour</th>
                            <th>Description</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $bdd = Database::connect();
                            $activite = $bdd->query('SELECT * FROM activites WHERE id_jour=3');
                            while ($donnees = $activite->fetch()) {
                                ?>
                                <tr>
                                    <td><?php echo $donnees['heure']; ?></td>
                                    <td><?php echo $donnees['title']; ?></td>
                                    <td><?php echo $donnees['id_jour']; ?></td>
                                    <td><?php echo $donnees['description']; ?></td>
                                    <td>
                                        <a href="update-activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-info">modifier</button>
                                        </a>
                                        <a href="delete_activite.php?id=<?php echo $donnees['id']; ?>">
                                            <button class="btn btn-danger">supprimer</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                            Database::disconnect();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <h3>Liste des activites du samedi</h3>
                        <table class="table table-bordered">
                            <thead>
                            <th>Heure</th>
                            <th>Titre</th>
                            <th>Jour</th>
                            <th>Description</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $bdd = Database::connect();
                            $activite = $bdd->query('SELECT * FROM activites WHERE id_jour=4');
                            while ($donnees = $activite->fetch()) {
                                ?>
                                <tr>
                                    <td><?php echo $donnees['heure']; ?></td>
                                    <td><?php echo $donnees['title']; ?></td>
                                    <td><?php echo $donnees['id_jour']; ?></td>
                                    <td><?php echo $donnees['description']; ?></td>
                                    <td>
                                        <button class="btn btn-info">modifier</button>
                                        <button class="btn btn-danger">supprimer</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            Database::disconnect();
                            ?>
                            </tbody>
                        </table>
                    </div>


            </section>
            <!-- /Content -->
            <!-- Header -->
            <?php include "./partials/footer.php"; ?>
            <!-- /Header -->
        </section>
        <!-- /Main Wrapper -->
    </div>
    <!-- /Body Wrap  -->
    <!-- Scripts -->
    <!-- jQuery -->
    <script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
    <script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>
    <!-- AnimatedSVGIcons -->
    <script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>
    <!-- Scroll -->
    <script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- HighCharts Plugin -->
    <script src="assets/js/plugins/highcharts/highcharts.js"></script>
    <!-- Magnific Popup Plugin -->
    <script src="assets/js/plugins/magnific/jquery.magnific-popup.js"></script>
    <!-- Plugins -->
    <script src="assets/js/plugins/c3charts/d3.min.js"></script>
    <script src="assets/js/plugins/c3charts/c3.min.js"></script>
    <script src="assets/js/plugins/circles/circles.js"></script>
    <!-- Jvectormap JS -->
    <script src="assets/js/plugins/jvectormap/jquery.jvectormap.min.js"></script>
    <script src="assets/js/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>
    <script src="assets/js/plugins/jvectormap/assets/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Theme Scripts -->
    <script src="assets/js/utility/utility.js"></script>
    <script src="assets/js/demo/demo.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/demo/widgets_sidebar.js"></script>
    <script src="assets/js/pages/dashboard1.js"></script>
    <script src="assets/js/demo/widgets.js"></script>
    <!-- /Scripts -->
</body>
</html>

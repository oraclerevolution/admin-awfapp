<?php
require './database.php';
$name = $email = $message = $entetes = "";
$sujet = "Réponse à votre demande de réseautage";
$isUploadSuccess=true;

function inputVerify($var){

    $var = trim($var);
    $var = htmlspecialchars($var);
    $var = stripslashes($var);

    return $var;
}

if (!empty($_POST)) {

    $name = inputVerify($_POST['name']);
    $email = inputVerify($_POST['email']);
    $message = inputVerify($_POST['mytextarea']);
    // en-têtes expéditeur
    $entetes = "From : africawebfestival@gmail.com\n";
    // type de contenu et encodage
    $entetes .= "Content-type: text/plain; charset=utf-8\n";

    if($isUploadSuccess){
        $db = Database::connect();
        $statement = $db->prepare("INSERT INTO messages_sent (name, email, message) VALUES (?, ?, ?)");
        $statement->execute(array($name, $email, $message));
        Database::disconnect();
        if(!mail($email, $sujet, $message, $entetes)){
            echo "erreur lors de l'envoi du mail";
        }else{
            header('Location: list_reseautage.php');
        }
    }
}

?>